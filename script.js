var Row = null;

function isiFormData() {
    var formData = {};
    formData["namab"] = document.getElementById("namab").value;
    formData["jumlahb"] = document.getElementById("jumlahb").value;
    return formData;
}

function baruFormData(data) {
    var table = document.getElementById("tableList").getElementsByTagName('tbody')[0];
    var newRow = table.insertRow(table.length);
    cell1 = newRow.insertCell(0);
        cell1.innerHTML = data.namab;
    cell2 = newRow.insertCell(1);
        cell2.innerHTML = data.jumlahb;
    cell3 = newRow.insertCell(2);
        cell3.innerHTML = 
        `<button onClick="Edit(this)">Edit</button>
        <button onClick="Delete(this)">Hapus</button>`;
    cell4 = newRow.insertCell(3);
        cell4.innerHTML = 
        '<input type="checkbox" class="check" name="check"></input>';
}

function upData(formData) {
    Row.cells[0].innerHTML = formData.namab;
    Row.cells[1].innerHTML = formData.jumlahb;
}

function SubmitForm(e) {
    event.preventDefault();
        var formData = isiFormData();
        if (Row == null){
            baruFormData(formData);
        }
        else{
            upData(formData);
        }
        Reset();    
}

function Edit(td) {
    Row = td.parentElement.parentElement;
    document.getElementById("namab").value = Row.cells[0].innerHTML;
    document.getElementById("jumlahb").value = Row.cells[1].innerHTML;
}

function Delete(td) {
    if (confirm('Seluruh data pada baris ini akan terhapus')) {
        row = td.parentElement.parentElement;
        document.getElementById('tableList').deleteRow(row.rowIndex);
        Reset();
    }
}

function Reset() {
    document.getElementById("namab").value = '';
    document.getElementById("jumlahb").value = '';
    Row = null;
}
